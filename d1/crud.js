let http = require("http");

// Mock Database

let directory = [
	{
		"name": "Ash Ketchum",
		"email": "besttrainer@mail.com"
	},
	{
		"name": "Jose Masipag",
		"email": "bawalangtamad@mail.com"
	},
	{
		"name": "Hanamichi Sakuragi",
		"email": "reboundislife@mail.com"
	},
]

http.createServer(function(request, response){
	if(request.url == "/" && request.method == "GET"){

		response.writeHead(200, {"Content-Type": "text-plain"});

		response.end("This is a response to a GET method request for the / endpoint.");

	}else if(request.url == "/" && request.method == "POST"){

		response.writeHead(200, {"Content-Type": "text-plain"});

		response.end("This is a response to a POST method request for the / endpoint.");
	}else if(request.url == "/" && request.method == "PUT"){

		response.writeHead(200, {"Content-Type": "text-plain"});

		response.end("This is a response to a PUT method request for the / endpoint.");

	}else if(request.url == "/" && request.method == "DELETE"){

		response.writeHead(200, {"Content-Type": "text-plain"});

		response.end("This is a response to a DELETE method request for the / endpoint.");

	}

	// Get Users

	// Route for returning all users upon receiving a GET request

	else if(request.url == "/users" && request.method == "GET"){

		// set the status code to 200, denoting ok
		// set response output to JSON data type
		response.writeHead(200,{"Content-Type": "application/json"});

		// input has to be data type STRING hence we use the JSON.stringify() method
		// this string input will be converted to desired output data type which has been set to JSON

		response.write(JSON.stringify(directory));

		response.end();
	}

	// Route to add a new user
		// we have to receive an input from the client

	else if(request.url == "/users" && request.method == "POST"){

		// In NodeJS, this is done in two steps

		// This will act as a placeholder for the resource/data to be created by the client later on 
		let requestBody = "";

		// 1st step - data step
			// data step will read the incoming stream of data from the client and process it so we can save it in the requestBody variable

		request.on("data", function(userData){

			requestBody += userData;

			console.log(userData);

		});

		// end step - this will run once or after the request data has been completely sent from the client

		request.on("end", function(){

			requestBody = JSON.parse(requestBody);

			console.log(requestBody);

			directory.push(requestBody);

			console.log(directory);

			response.writeHead(200,{"Content-Type": "application/json"});
			response.end(JSON.stringify(directory));

		});

	}



}).listen(4000);

console.log("Server running at localhost:4000 (crud.js)")